package com.example.alcodesonboard2.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.alcodesonboard2.FriendDataHolder;
import com.example.alcodesonboard2.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FriendAdapter extends RecyclerView.Adapter<FriendAdapter.ViewHolder> {

    private static FragmentActivity activity;
    private final ArrayList<FriendDataHolder> mFriends;
    private Callbacks mCallbacks;

    public FriendAdapter(ArrayList<FriendDataHolder> mFriends, FragmentActivity activity) {
        this.mFriends = mFriends;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mFriends.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        if (mFriends != null) {
            return mFriends.size();
        } else {
            return 0;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public interface Callbacks {

        void onListItemClicked(FriendDataHolder data);

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_root)
        public LinearLayout root;

        @BindView(R.id.e_mail)
        public TextView email;

        @BindView(R.id.first_name)
        public TextView firstName;

        @BindView(R.id.last_name)
        public TextView lastName;

        @BindView(R.id.avatar)
        public ImageView avatar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }

        public void bindTo(FriendDataHolder data, Callbacks callbacks) {
            resetViews();

            if (data != null) {

                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.ic_launcher_background);

                Glide.with(activity)
                        .load(data.getAvatar())
                        .apply(requestOptions)
                        .into(avatar);

                email.setText(data.getEmail());
                firstName.setText(data.getFirstname());
                lastName.setText(data.getLastname());
                root.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callbacks.onListItemClicked(data);
                    }
                });
            }
        }

        public void resetViews() {
            email.setText("");
            firstName.setText("");
            lastName.setText("");
            avatar.setImageResource(R.drawable.ic_launcher_foreground);
            root.setOnClickListener(null);
        }

    }
}
