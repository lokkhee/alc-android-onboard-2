package com.example.alcodesonboard2.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.alcodesonboard2.R;
import com.example.alcodesonboard2.fragments.FriendDetailsFragment;

import butterknife.ButterKnife;

public class FriendDetailsActivity extends AppCompatActivity {

    public static final String EXTRA_LONG_FRIEND_ID = "EXTRA_LONG_FRIEND_ID";

    public static final int RESULT_CONTENT_MODIFIED = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_frienddetails);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(FriendDetailsFragment.TAG) == null) {
            // Init fragment.
            Intent intent = getIntent();
            Bundle extras = intent.getExtras();

            String email = extras.getString("EMAIL");
            String first = extras.getString("FIRST");
            String last = extras.getString("LAST");
            String avatar = extras.getString("AVATAR");
            long friendId = 0;

            if (intent != null) {
                friendId = intent.getLongExtra(EXTRA_LONG_FRIEND_ID, 0);
            }

            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, FriendDetailsFragment.newInstance(email, first, last, avatar), FriendDetailsFragment.TAG)
                    .commit();

        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
