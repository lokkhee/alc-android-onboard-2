package com.example.alcodesonboard2.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.alcodesonboard2.R;
import com.example.alcodesonboard2.fragments.LoginFragment;
import com.example.alcodesonboard2.fragments.TermFragment;

import butterknife.ButterKnife;

public class TermActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_term);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(LoginFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, TermFragment.newInstance(), TermFragment.TAG)
                    .commit();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
