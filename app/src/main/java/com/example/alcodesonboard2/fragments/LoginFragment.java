package com.example.alcodesonboard2.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.alcodesonboard2.BuildConfig;
import com.example.alcodesonboard2.R;
import com.example.alcodesonboard2.activities.MainActivity;
import com.example.alcodesonboard2.activities.TermActivity;
import com.example.alcodesonboard2.gsonmodels.LoginModel;
import com.example.alcodesonboard2.utils.NetworkHelper;
import com.example.alcodesonboard2.utils.SharedPreferenceHelper;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getSimpleName();

    @BindView(R.id.edittext_email)
    protected EditText mEditTextEmail;
    @BindView(R.id.edittext_password)
    protected EditText mEditTextPassword;
    @BindView(R.id.button_login)
    protected Button mButtonLogin;
    @BindView(R.id.textview_terms)
    protected TextView mTextView;

    private Unbinder mUnbinder;

    public LoginFragment() {
    }

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (BuildConfig.DEBUG) {
            // Auto fill in credential for testing purpose.
            mEditTextEmail.setText("eve.holt@reqres.in");
            mEditTextPassword.setText("cityslicka");
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.button_login)
    protected void doLogin() {

        if (!isNetworkConnected()) {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Network Connection Error")
                    .setMessage("No Internet Connection Detected!")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .show();
        } else {

            // Disable button and wait for server response.
            mButtonLogin.setEnabled(false);

            mButtonLogin.setText(R.string.btn_text_loading); //Change the text to loading

            final String email = mEditTextEmail.getText().toString().trim();
            final String password = mEditTextPassword.getText().toString(); // Password should not trim.

            if (email.isEmpty() || password.isEmpty()) {
                if (email.isEmpty())
                    mEditTextEmail.setError("This field cannot be left blank!"); //text box error message
                else
                    mEditTextPassword.setError("This field cannot be left blank!"); //text box error message

                new AlertDialog.Builder(getActivity())
                        .setTitle("Invalid Field")
                        .setMessage("This field cannot be left blank!")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();

                mButtonLogin.setEnabled(true); //Re-enable the button
                mButtonLogin.setText(R.string.btn_text_login); //Change the text back to Login
                return;
            }
            // Call login API.
            String url = BuildConfig.BASE_API_URL + "login";
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                        Toast.makeText(getActivity(), "Login success.", Toast.LENGTH_SHORT).show();
                        // Convert JSON string to Java object.
                        LoginModel responseModel = new GsonBuilder().create().fromJson(response, LoginModel.class);

                        // Save user's email to shared preference.
                        SharedPreferenceHelper.getInstance(getActivity())
                                .edit()
                                .putString("email", email)
                                .putString("token", responseModel.token)
                                .apply();

                        startActivity(new Intent(getActivity(), MainActivity.class));
                    }
                    getActivity().finish();
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Timber.e("d;;Login error: %s", error.getMessage());

                    if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {

                        // Show error in popup dialog.
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Error")
                                .setMessage("Invalid email or password.")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                                .show();
                    }

                    mButtonLogin.setEnabled(true);
                    mButtonLogin.setText(R.string.btn_text_login); //Change the text back to Login
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", email);
                    params.put("password", password);

                    return params;
                }
            };

            NetworkHelper.getRequestQueueInstance(getActivity()).add(stringRequest);

        }
    }

    @OnClick(R.id.textview_terms)
    protected void openTermOfUse() {
        startActivity(new Intent(getActivity(), TermActivity.class));
        //getActivity().finish();
    }

}
