package com.example.alcodesonboard2.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.alcodesonboard2.FriendDataHolder;
import com.example.alcodesonboard2.R;
import com.example.alcodesonboard2.activities.FriendDetailsActivity;
import com.example.alcodesonboard2.activities.LoginActivity;
import com.example.alcodesonboard2.adapter.FriendAdapter;
import com.example.alcodesonboard2.utils.SharedPreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainFragment extends Fragment implements FriendAdapter.Callbacks {

    public static final String TAG = MainFragment.class.getSimpleName();
    private final int REQUEST_CODE_FRIEND_DETAIL = 300;

    @BindView(R.id.recyclerview)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.textview_nodata)
    protected TextView mTextView;

    ArrayList<FriendDataHolder> friendData = new ArrayList();

    private Unbinder mUnbinder;
    private FriendAdapter mFriendAdapter;


    public MainFragment() {
    }

    public static Fragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true); //Menu for Logout button

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initFriend();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemID = item.getItemId();

        if (itemID == R.id.menu_logout) {
            //show confirm dialog before logout
            new AlertDialog.Builder(getActivity())
                    .setTitle("Confirmation")
                    .setMessage("Are you sure you want to logout?")
                    .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Logout user.
                            // Clear all user's data.
                            SharedPreferenceHelper.getInstance(getActivity())
                                    .edit()
                                    .clear()
                                    .apply();

                            // Go to login page.
                            startActivity(new Intent(getActivity(), LoginActivity.class));
                            getActivity().finish();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    })
                    .show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClicked(FriendDataHolder data) {

        if (Integer.parseInt(data.getId()) > 0) {
            Intent intents = new Intent(getActivity(), FriendDetailsActivity.class);
            Bundle args = new Bundle();

            args.putString("EMAIL", data.getEmail());
            args.putString("FIRST", data.getFirstname());
            args.putString("LAST", data.getLastname());
            args.putString("AVATAR", data.getAvatar());

            intents.putExtras(args);
            startActivity(intents);

        } else {
            Intent intents = new Intent(getActivity(), FriendDetailsActivity.class);
            Bundle args = new Bundle();
            intents.putExtra(FriendDetailsActivity.EXTRA_LONG_FRIEND_ID, data.getId());
            startActivity(intents);
        }
    }

    private void initView() {
        mFriendAdapter = new FriendAdapter(friendData, getActivity());
        mFriendAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mFriendAdapter);

        //Check if there is any data inside RecyclerView, show No Data label if none
        //TODO check for empty
        if (mFriendAdapter.getItemCount() == 0) {
            mRecyclerView.setVisibility(View.GONE);
            mTextView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mTextView.setVisibility(View.GONE);
        }

    }

    private ArrayList<FriendDataHolder> initFriend() {

        //TODO load data from resreq
        String url = "https://reqres.in/api/users?page=2";
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String id = jsonObject.getString("id");
                        String email = jsonObject.getString("email");
                        String firstName = jsonObject.getString("first_name");
                        String lastName = jsonObject.getString("last_name");
                        String avatar = jsonObject.getString("avatar");
                        friendData.add(new FriendDataHolder(id, email, firstName, lastName, avatar));
                        friendData.get(i).getEmail();
                    }
                    initView();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        requestQueue.add(jsonObjectRequest);

        //TODO to ask

        //Run first send request
        //return -> data size = 0 , set No Data label
        //then run back this segment and get data, but recycler view is ady gone
        return friendData;
    }

}
