package com.example.alcodesonboard2.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.alcodesonboard2.FriendDataHolder;
import com.example.alcodesonboard2.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FriendDetailsFragment extends Fragment {

    public static final String TAG = FriendDetailsFragment.class.getSimpleName();

    private static final String ARG_LONG_FRIEND_ID = "ARG_LONG_FRIEND_ID";

    @BindView(R.id.textview_email)
    protected TextView mTextViewEmail;
    @BindView(R.id.textview_first_name)
    protected TextView mTextViewFirstName;
    @BindView(R.id.textview_last_name)
    protected TextView mTextViewLastName;
    @BindView(R.id.profile)
    protected ImageView mImageViewProfile;

    private Unbinder mUnbinder;
    private Long mFriendId = 0L;

    private String email;
    private String first;
    private String last;
    private String avatar;


    public FriendDetailsFragment() {
    }

    public static FriendDetailsFragment newInstance(String email, String first, String last, String avatar) {
        Bundle args = new Bundle();
        args.putString("EMAIL", email);
        args.putString("FIRST", first);
        args.putString("LAST", last);
        args.putString("AVATAR", avatar);

        FriendDetailsFragment fragment = new FriendDetailsFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_frienddetails, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //check args passed from previous page.
        Bundle args = getArguments();

        if (args != null) {
            email = args.getString("EMAIL");
            first = args.getString("FIRST");
            last = args.getString("LAST");
            avatar = args.getString("AVATAR");
        }

        initView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

    }

    private void initView() {
        //TODO load single user
        ArrayList<FriendDataHolder> list = new ArrayList<FriendDataHolder>();

        if (list != null) {
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.ic_launcher_background);

            Glide.with(getActivity())
                    .load(avatar)
                    .apply(requestOptions)
                    .into(mImageViewProfile);

            mTextViewEmail.setText(email);
            mTextViewFirstName.setText(first);
            mTextViewLastName.setText(last);
        }

    }
}
