package com.example.alcodesonboard2.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferenceHelper {

    private static SharedPreferences mInstance;

    private SharedPreferenceHelper() {
    }

    public static SharedPreferences getInstance(Context context) {
        if (mInstance == null) {
            synchronized (SharedPreferenceHelper.class) {
                if (mInstance == null) {
                    mInstance = context.getSharedPreferences("app", Context.MODE_PRIVATE);
                }
            }
        }

        return mInstance;
    }
}
